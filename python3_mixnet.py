import socket               # Import socket module
from cryptography.hazmat.primitives import padding
import os
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization

# needed for OAEP padding
from cryptography.hazmat.primitives.asymmetric.padding import OAEP
from cryptography.hazmat.primitives.asymmetric.padding import MGF1
from cryptography.hazmat.primitives import hashes

PORT = 57599

recipient = "Dave"			#Name of the receiver
group = "14"				#Group number
message  = recipient + "," + group

padder1 = padding.PKCS7(128).padder()			#AES key size is 128b
padder2 = padding.PKCS7(128).padder()
padder3 = padding.PKCS7(128).padder()

__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__))) # this file location path

# Load the public key files
with open(os.path.join(__location__, 'public-key-mix-1.pem'), "rb") as key_file1:		#the key is in the same directory as the script
     public_key1 = serialization.load_pem_public_key(
         key_file1.read(),
         backend=default_backend()
     )
     
with open(os.path.join(__location__, 'public-key-mix-2.pem'), "rb") as key_file2:		#the key is in the same directory as the script
     public_key2 = serialization.load_pem_public_key(
         key_file2.read(),
         backend=default_backend()
     )
		
with open(os.path.join(__location__, 'public-key-mix-3.pem'), "rb") as key_file3:		#the key is in the same directory as the script
     public_key3 = serialization.load_pem_public_key(
         key_file3.read(),
         backend=default_backend()
     )

backend = default_backend()
# Generate random keys and IVs
key1 = os.urandom(16)
iv1 = os.urandom(16)
key2 = os.urandom(16)
iv2 = os.urandom(16)
key3 = os.urandom(16)
iv3 = os.urandom(16)

cipher3 = Cipher(algorithms.AES(key1), modes.CBC(iv1), backend=backend)
cipher2 = Cipher(algorithms.AES(key2), modes.CBC(iv2), backend=backend)
cipher1 = Cipher(algorithms.AES(key3), modes.CBC(iv3), backend=backend)

encryptor1 = cipher1.encryptor()
encryptor2 = cipher2.encryptor()
encryptor3 = cipher3.encryptor()

#------------------------------------------------------ First layer
padded_message1 = padder1.update(message.encode('utf-8'))
padded_message1 += padder1.finalize()

aes_ciphertext1 = encryptor1.update(padded_message1)
#aes_ciphertext1 += encryptor1.finalize()

rsa_message1 = iv3 + key3

rsa_ciphertext1 = public_key3.encrypt(rsa_message1,
	OAEP(
		mgf = MGF1(algorithm=hashes.SHA1()),
		algorithm=hashes.SHA1(),
		label=None
		)
		)
#rsa_ciphertext1 = PKCS1_OAEP.new(public_key3).encrypt(rsa_message1)
	
E1 = rsa_ciphertext1 + aes_ciphertext1

#------------------------------------------------------ Second layer
padded_message2 = padder2.update(E1)
padded_message2 += padder2.finalize()

aes_ciphertext2 = encryptor2.update(padded_message2) + encryptor2.finalize()

rsa_message2 = iv2 + key2
rsa_ciphertext2 = public_key2.encrypt(rsa_message2,
	OAEP(
		mgf = MGF1(algorithm=hashes.SHA1()),
		algorithm=hashes.SHA1(),
		label=None
		)
		)
	
E2 = rsa_ciphertext2 + aes_ciphertext2

#------------------------------------------------------ Third layer
padded_message3 = padder3.update(E2)
padded_message3 += padder3.finalize()

aes_ciphertext3 = encryptor3.update(padded_message3) + encryptor3.finalize()

rsa_message3 = iv1 + key1
rsa_ciphertext3 = public_key1.encrypt(rsa_message3,
	OAEP(
		mgf = MGF1(algorithm=hashes.SHA1()),
		algorithm=hashes.SHA1(),
		label=None
		)
		)
	
E3 = rsa_ciphertext3 + aes_ciphertext3

#------------------------------------------------------

msg_length = len(E3).to_bytes(4, byteorder = 'big')

E3 = msg_length + E3

s = socket.socket()        					 # Create a socket object
s.connect(('pets.ewi.utwente.nl', PORT))    # Set name and port
s.sendall(E3)								 # Finally send the message
#response = s.recv(16)

s.close() 
